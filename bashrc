#!/bin/bash

export ARCHFLAGS='-arch x86_64'
export PS1="\[\033[38;5;71m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]:\[$(tput sgr0)\]\[\033[38;5;203m\]\W\[$(tput sgr0)\]\[\033[38;5;15m\]\\$ \[$(tput sgr0)\]"
export CLICOLOR='1'
export TERM='xterm-256color'
export VISUAL='vim'

# Vim editor's configuration
export EDITOR="${VISUAL}"
export LC_ALL='en_US.UTF-8'
export LANG='en_US.UTF-8'
export PATH="$HOME/bin:$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH:/.local/bin"

function source_if_exists() {
  file_to_source="${1}"
  [ -r "$file_to_source" ] && source "${file_to_source}" || true
}

source_if_exists "${HOME}/.iterm2_shell_integration.bash"
source_if_exists "/usr/local/etc/bash_completion"
source_if_exists "${HOME}/.fzf.bash"
source_if_exists "${HOME}/.bash/alias"
